php_image = registry.gitlab.com/dkx/docker/php/cli:7.4.1

.PHONY: deps-install
deps-install:
	docker run --rm -v `pwd`:/app $(php_image) composer install
	docker run --rm -v `pwd`:/app $(php_image) composer run-script run-phar-installer
	docker run --rm -v `pwd`:/app $(php_image) composer run-script fetch-composer

.PHONY: parallel-lint
parallel-lint:
	docker run --rm -v `pwd`:/app $(php_image) vendor/bin/phplint

.PHONY: cs
cs:
	docker run --rm -v `pwd`:/app $(php_image) vendor/bin/phpcs

.PHONY: cs-fix
cs-fix:
	docker run --rm -v `pwd`:/app $(php_image) vendor/bin/phpcbf

.PHONY: metrics
metrics:
	docker run --rm -v `pwd`:/app $(php_image) php tools/phpmetrics.phar --report-html=phpmetrics --exclude=vendor .

.PHONY: copy-paste-detection
copy-paste-detection:
	docker run --rm -v `pwd`:/app $(php_image) php tools/phpcpd.phar --fuzzy src

.PHONY: loc
phploc:
	docker run --rm -v `pwd`:/app $(php_image) php tools/phploc.phar src
