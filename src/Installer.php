<?php

declare(strict_types=1);

namespace DKX\ComposerPharInstaller;

use Composer\IO\IOInterface;
use function copy;
use function is_dir;
use function mkdir;
use const DIRECTORY_SEPARATOR;

final class Installer
{
	private IOInterface $io;

	private string $targetPath;

	public function __construct(IOInterface $io, string $targetPath)
	{
		$this->io         = $io;
		$this->targetPath = $targetPath;
	}

	/**
	 * @param string[] $list
	 */
	public function process(array $list) : void
	{
		@mkdir($this->targetPath);

		if (! is_dir($this->targetPath)) {
			$this->io->writeError('Could not create directory "' . $this->targetPath . '" for phar packages');

			return;
		}

		foreach ($list as $name => $url) {
			$this->fetchPhar($name, $url);
		}
	}

	private function fetchPhar(string $name, string $url) : void
	{
		$this->io->write('Fetching ' . $name . ' from ' . $url . '...', false);
		copy($url, $this->targetPath . DIRECTORY_SEPARATOR . $name . '.phar');
		$this->io->write(' done');
	}
}
