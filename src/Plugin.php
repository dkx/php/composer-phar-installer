<?php

declare(strict_types=1);

namespace DKX\ComposerPharInstaller;

use Composer\Composer;
use Composer\EventDispatcher\EventSubscriberInterface;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;
use Composer\Script\Event;
use Composer\Script\ScriptEvents;
use function array_key_exists;
use function dirname;
use const DIRECTORY_SEPARATOR;

final class Plugin implements PluginInterface, EventSubscriberInterface
{
	private const DEFAULT_DIR = 'tools';

	/**
	 * @return string[]
	 */
	public static function getSubscribedEvents() : array
	{
		return [
			ScriptEvents::POST_UPDATE_CMD => 'run',
			ScriptEvents::POST_INSTALL_CMD => 'run',
		];
	}

	public static function runFromSelf(Event $event) : void
	{
		(new static())->run($event);
	}

	public function activate(Composer $composer, IOInterface $io) : void
	{
	}

	public function run(Event $event) : void
	{
		$composer = $event->getComposer();
		$package  = $composer->getConfig()->getConfigSource()->getName();

		$extra = $composer->getPackage()->getExtra();
		$extra = array_key_exists('install-phar', $extra) ? $extra['install-phar'] : [];

		$dir = dirname($package) . DIRECTORY_SEPARATOR . ($extra['dir'] ?? self::DEFAULT_DIR);

		$installer = new Installer($event->getIO(), $dir);
		$installer->process($extra['require'] ?? []);
	}
}
