# DKX/ComposerPharInstaller

Composer phar installer plugin

## Installation

```bash
$ composer require dkx/composer-phar-installer
```

## Usage

Update your `composer.json` to install eg. phpunit:

```json
{
    "name": "my/app",
    "extra": {
        "install-phar": {
            "dir": "tools",
            "require": {
                "phpunit": "https://phar.phpunit.de/phpunit-8.5.2.phar"
            }
        }
    },
    "autoload": {
        "psr-4": {
            "MyApp\\": "./src"
        }
    },
    "require": {
        "php": ">=7.4"
    }
}
```

Now every time you run `composer install` or `composer update` phpunit will be automatically fetched and saved as 
`./tools/phpunit.phar`.
